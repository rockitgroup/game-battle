package com.rockitgroup.socket.server.game.battle.interfaces;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.scheduling.annotation.EnableAsync;

@Slf4j
@EnableAsync
@SpringBootApplication(exclude = {
        DataSourceAutoConfiguration.class,
})
@ImportResource({
        "classpath:spring/beanconfig/${" + AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME + "}/application-context.xml"
})
public class GameBattleServerApplicationInitializer {

    public static void main(String[] args) {
        SpringApplication.run(GameBattleServerApplicationInitializer.class, args);
    }
}
