CREATE TABLE `items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) NOT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `buyPrice` int(10) NOT NULL COMMENT 'Buy price of item for game currency',
  `sellPrice` int(10) DEFAULT NULL COMMENT 'Sell price of item for game currency',
  `gameId` bigint(20) NOT NULL,
  `status` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `account_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `accountId` bigint(20) NOT NULL,
  `itemId` bigint(20) NOT NULL,
  `status` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `item_attributes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `itemId` bigint(20) NOT NULL,
  `attributeKey` varchar(100) NOT NULL,
  `attributeValue` varchar(300) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `leagues` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gameId` bigint(20) NOT NULL,
  `name` varchar(300) NOT NULL,
  `higherLeagueId` bigint(20) DEFAULT NULL COMMENT 'the league for player being promoted up to. Null if highest level league',
  `lowerLeagueId` bigint(20) DEFAULT NULL COMMENT 'the league for player being delegated down to. Null if lowest level league',
  `maxTotalRank` int(10) NOT NULL,
  `promotionTopRange` int(10) NOT NULL COMMENT 'the first X rank will be promoted',
  `delegationBottomRange` int(10) NOT NULL COMMENT 'the last X rank will be delegated',
  `status` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `league_rankings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `leagueId` bigint(20) NOT NULL,
  `accountId` bigint(20) NOT NULL,
  `currentRank` int(10) NOT NULL,
  `previousRank` int(10) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `league_rankings_udx01` (`leagueId`,`accountId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `league_rewards` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `leagueId` bigint(20) NOT NULL,
  `rankFrom` int(10) NOT NULL COMMENT 'inclusive from',
  `rankTo` int(10) NOT NULL COMMENT 'inclusive to',
  `rewardAmount` int(10) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


