#!/bin/bash

APP_NAME=game-data
APP_SERVER=$APP_NAME-interfaces
JAVA=java
JAVA_OPTS="-Dfile.encoding=UTF-8 -Xmx256m -Xms128M -XX:MaxDirectMemorySize=256M -XX:-OmitStackTraceInFastThrow"
ROOT_DIR=/home/dtn1712
HOME=$ROOT_DIR/$APP_NAME
STAGE=dev
PID=$HOME/$APP_NAME.pid

export JAVA_OPTS=$JAVA_OPTS
export SPRING_PROFILES_ACTIVE=$STAGE
export GRADLE_OPTS="-Dorg.gradle.daemon=false"

source $ROOT_DIR/.bashrc

cd $HOME

# See how we were called.
case "$1" in
 start)
  git reset --hard HEAD && git pull origin $STAGE && ./gradlew :$APP_SERVER:clean
  ps -ef | grep $APP_SERVER | grep -v grep | awk '{print $2}' | xargs kill
  ./gradlew :$APP_SERVER:bootRun &
  echo $! > $PID
  ;;
 stop)
  ps -ef | grep $APP_SERVER | grep -v grep | awk '{print $2}' | xargs kill
  ;;
*)
 echo "Usage: $APP_NAME {start\|stop}";;
esac
exit 0
